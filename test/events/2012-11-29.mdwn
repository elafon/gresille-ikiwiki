[[!meta title="I am NOT a nut...."]]
[[!meta date="2012-11-29 18:30:00"]]

hacker, n.:
	Originally, any person with a knack for coercing stubborn inanimate
	things; hence, a person with a happy knack, later contracted by the
	mythical philosopher Frisbee Frobenius to the common usage, 'hack'.
	In olden times, upon completion of some particularly atrocious body
	of coding that happened to work well, culpable programmers would gather
	in a small circle around a first edition of Knuth's Best Volume I by
	candlelight, and proceed to get very drunk while sporadically rending
	the following ditty:

		Hacker's Fight Song

		He's a Hack!  He's a Hack!
		He's a guy with the happy knack!
		Never bungles, never shirks,
		Always gets his stuff to work!

All take a drink (important!)
