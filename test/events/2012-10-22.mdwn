[[!meta title="Slippery when wet."]]
[[!meta date="2012-10-22 18:30:00"]]

...computer hardware progress is so fast.  No other technology since
civilization began has seen six orders of magnitude in performance-price
gain in 30 years.
		-- Fred Brooks, Jr.
