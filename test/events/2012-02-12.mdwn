[[!meta title="I demand IMPUNITY!"]]
[[!meta date="2012-02-12 18:30:00"]]

"Just in terms of allocation of time resources, religion is not very
 efficient. There's a lot more I could be doing on a Sunday morning."
                       [Bill Gates]
