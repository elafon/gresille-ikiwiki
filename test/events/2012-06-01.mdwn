[[!meta title="Truckers welcome."]]
[[!meta date="2012-06-01 18:30:00"]]

"The use of the astrological zodiac in the Bible has been well established,
 and without much question, the twelve zodiacal gods have come down to us as
 the twelve apostles. The shepherd's crook used by the Egyptians' Osiris was
 used for the bishops' and popes' crozier. They transformed his ankh, the
 phallic sign of life, into the Christian cross, and they copied the high-
 pointed headdress of Osiris as their prototype for Saint Peter's papal tiara.
 There could only be four gospels written. The reason there are only four
 biblical gospels was because Saint Jerome believed in the four cardinal gods
 of the zodiac. But who was father of the four "cardinal" gods of the zodiac?
 Yes, indeed! It was the divine Egyptian son Horus, whose birthday was on the
 25th of December long before there was a biblical Jesus."
       [Dr. Wally F. Dean, "The Mania of Religion", 1995, p. 106]
