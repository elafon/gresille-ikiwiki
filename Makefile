PLUGINS = addtag compile datetime_cmp jscalendar monthcalendar pageversion parenttag poetry redirect sidebar2 taskreport verboserpc
UNMAINENED = created_in_future evariste

all: doc

.PHONY: .FORCE
doc:
	ikiwiki --setup wiki.setup --rebuild --verbose

refresh:
	ikiwiki --setup wiki.setup --refresh --verbose
