[[!meta title="Grésille-Ikiwiki — A rag-bag of Ikiwiki plugins"]]

## Installation

To use those plugins, in your setup file, set the [`libdirs`](https://ikiwiki.info/plugins/install/) option to use the plugin directory, as well as the [`underlays`](http://ikiwiki.info/plugins/underlay/) option to use the `PLUGIN/underlays` directory.

The documentation is an IkiWiki repository using the Grésille-IkiWiki plugins.
Just look at its configuration file `doc/wiki.setup` to see how it refers to the plugins.

# Source code, issue tracker, etc.

[[https://framagit.org/spalax/gresille-ikiwiki]]

## List of plugins

[[!map pages="tagged(plugin)"]]

## Licence

Copyright (c) 2012-2016 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
