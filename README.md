# Grésille-IkiWiki — A rag-bag of ikiwiki plugins

This is the repository of my Ikiwiki plugins, written for the website of [Grésille](https://www.gresille.org/) and my profesionnal website (which no longer use Ikiwiki).

Those plugins are no longer maintained. If you still have a question, you migth open an issue here, or ask a nice soul on [Ikiwiki.info](https://ikiwiki.info). Good luck!

## Documentation

To build the documentation, in the root of the repository, run:

    make

The compiled documentation is available at: https://spalax.frama.io/gresille-ikiwiki.

## Licence

Copyright (c) 2012-2016 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
