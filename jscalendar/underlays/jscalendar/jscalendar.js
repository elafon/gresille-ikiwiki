/*
 * Copyright (c) 2012-2014 Louis Paternault <spalax@gresille.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * Set of javascript functions to be used with IkiWiki plugin "jscalendar".
 */

function get_jscalendar_events(hash, year, month) {
	/*
	 * Return the events of the month and year given in argument.
	 * Month 0 is January.
	 */

	if (month < 9) {
		month = "0" + (month + 1);
	} else {
		month = "" + (month + 1);
	}

	// Download the right YEAR_MONTH.jscalendar file, which contains the list of
	// events.
	var xhr_object = null; 
	if(window.XMLHttpRequest) // Firefox 
		xhr_object = new XMLHttpRequest(); 
	else if(window.ActiveXObject) // Internet Explorer 
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
	else { // XMLHttpRequest non supporté par le navigateur 
		return false;
	} 

	xhr_object.open("GET", indexurl + 'jscalendar/' + hash + '/jscalendar-' + year + "-" + month + '.js', false);
	xhr_object.send(null); 
	if (xhr_object.status == 404) {
		return false;
	}

	return JSON.parse(xhr_object.responseText);
}

function get_month_url(hash, year, month) {
	/*
	 * Return the URL of the wiki page of the month of the given year.
	 * Note that "month" can be any integer, even
	 * negative or greater than 13. The -1th month of 2012
	 * is considered as the 11th month of 2011.
	 *
	 * Month 0 is January.
	 */
	while(month < 0){
		month += 12;
		year -= 1;
	}
	while(month > 11){
		month -= 12;
		year += 1;
	}

	month += 1; // Switch from "January is 0" to "January is 1"
	if (month < 10) {
		month = "0" + month;
	} else {
		month = "" + month;
	}
	return indexurl + options[hash].archivebase + year + "/" + month;
}

function jscalendar_month_link(hash, id, year, month, arrow) {
		return '<a href="#" onclick="jscalendar_set_month(\'' + hash + '\', \'' + id + '\',' + year + ',' + (month-1) + '); return false;" title="' + options[hash].month_name[(month + 12) % 12] + ' ' + year + '">' + arrow + '</a>';
}

function previous_month(hash, id, events, year, month) {
	/*
	 * Returns the HTML code providing a link to the previous month. Returns an
	 * empty string if there is no previous month.
	 */
	if (! events) {
		var month_list = options[hash].month_list;
		for (var i in month_list) {
			var previous = month_list[month_list.length - i - 1];
			if (previous < [year, month]) {
				return jscalendar_month_link(hash, id, previous[0], previous[1], "&#8592;");
			}
		}
		return "";
	} else if (events.adjacent[0]) {
		return jscalendar_month_link(hash, id, events.adjacent[0][0], events.adjacent[0][1] , "&#8592;");
	} else {
		return "";
	}
}

function next_month(hash, id, events, year, month) {
	/*
	 * Returns the HTML code providing a link to the next month. Returns an
	 * empty string if there is no next month.
	 */
	if (! events) {
		var month_list = options[hash].month_list;
		for (var i in month_list) {
			var next = month_list[i];
			if (next > [year, month]) {
				return jscalendar_month_link(hash, id, next[0], next[1], "&#8594;");
			}
		}
		return "";
	} else if (events.adjacent[1]) {
		return jscalendar_month_link(hash, id, events.adjacent[1][0], events.adjacent[1][1], "&#8594;");
	} else {
		return "";
	}
}

function get_jscalendar_month(hash, id, year, month) {
	/*
	 * Return the HTML code corresponding to the calendar of the month given in
	 * argument
	 */
	var events = get_jscalendar_events(hash, year, month);

	var cal = "";

	// Header: arrows and month name.
	cal += '<table class="month-' + options[hash].css_jscalendar_keyword + ' jscalendar-new">\n';
	cal += '<thead>\n';
	cal += '<tr>\n';
	cal += '<th class="month-' + options[hash].css_jscalendar_keyword + '-arrow">';
	// Previous month
	cal += previous_month(hash, id, events, year, month);
	cal += '</th>\n';
	var month_name = options[hash].month_name[month];
	if (month_name.length > 4) {
		month_name = month_name.slice(0,3) + ".";
	}
	month_name += ' ' + year;
	if (options[hash].month_link) {
		// According to option, we put or do not put a link to .../YEAR/MONTH.mdwn
		month_name = '<a href="' + get_month_url(hash, year, month) + '" title="' + options[hash].month_name[month] + '">' + month_name + '</a>';
	}
	cal += '<th class="month-' + options[hash].css_jscalendar_keyword + '-head" colspan="5">' + month_name + '</th>\n';
	cal += '<th class="month-' + options[hash].css_jscalendar_keyword + '-arrow">';
	// Next month
	cal += next_month(hash, id, events, year, month);
	cal += '</th>\n';
	cal += '</tr>\n';

	// Header: name of days.
	cal += '<tr>\n';
	for(var i = 0; i < 7; i += 1) {
		cal += '<th class="month-' + options[hash].css_jscalendar_keyword + '-day-head ' + options[hash].day_name[(options[hash].week_start_day + i) % 7] + '" title="' + options[hash].day_name[(options[hash].week_start_day + i) % 7] + '">' + options[hash].day_name[(options[hash].week_start_day + i) % 7][0] + '</th>\n';
	}
	cal += '</tr>\n';
	cal += '</thead>\n<tbody>';


	// List of days
	var day = new Date(year, month, 1 - (((new Date(year, month, 1)).getDay() - options[hash].week_start_day + 7) % 7));
	var today = new Date();

	while (day.getMonth() != (month + 1) % 12) {
		// Iteration in a month
		cal += '<tr>\n';
		for (var dow = 0; dow < 7; dow++) {
			// Iteration in a week
			var css_end = "noday";
			var content = "&nbsp;";
			if (events && (day.getDate() in events.content) && (day.getMonth() == month)) {
				css_end = "link";
				// Several events this day
				var desc = "";
				content = '<span class="popup">';
				content += day.getDate();
				content += '<span class="balloon">';
				content += '<ul>';
				for (var i in events.content[day.getDate()]) {
					desc += events.content[day.getDate()][i].title;
					content += '<li><a href="' + indexurl + events.content[day.getDate()][i].url + '">' + events.content[day.getDate()][i].title + '</a></li>';
					if (i + 1 != events.content[day.getDate()].length) {
						desc += " | ";
					}
				}
				content += '</ul>';
				content += '</span></span>';
			} else if (day.getMonth() == month) {
				css_end = "nolink";
				content = day.getDate();
			}
			if (day.getFullYear() == today.getFullYear() && (day.getMonth() == today.getMonth()) && (day.getDate() == today.getDate()) && (day.getMonth() == month)) {
				css_end = "this-day";
			}
			cal += '<td class="month-' + options[hash].css_jscalendar_keyword + '-day-' + css_end + " " + options[hash].day_name[day.getDay()] + '">' + content + '</td>\n';
			day.setDate(day.getDate() + 1);
		}
		cal += '</tr>\n';
	}

	cal += '</tbody>\n';
	cal += '</table>\n';

	return cal;
}

function jscalendar_parse_date(hash, id, year, month) {
	var today = new Date();
	if (year == "") {
		year = today.getFullYear();
	} else if (parseInt(year) < 0) {
		year = today.getFullYear() + parseInt(year);
	} else {
		year = parseInt(year);
	}
	if (month == "") {
		month = today.getMonth();
	} else if (parseInt(month) < 0) {
		/* jscalendar_set_month() will handle negative month. */
		month = today.getMonth() + parseInt(month);
	} else {
		month = parseInt(month) - 1;
	}
	return jscalendar_set_month(hash, id, year, month);
}

function jscalendar_set_month(hash, id, year, month) {
	/*
	 * Fill the div of class 'jscalendar-month' with a beautiful calendar.
	 *
	 * Month can be any integer, including negative or greater than 11.
	 * If, e.g., month = -2, this means November of (year-1).
	 */
	while(month < 0){
		month += 12;
		year -= 1;
	}
	while(month > 11){
		month -= 12;
		year += 1;
	}
	
	document.getElementById(id).innerHTML = get_jscalendar_month(hash, id, year, month);
	return false;
}

function initjscalendar(base, index, hash_list) {
	/*
	 * Called once at the end of page loading: populate options for all calendars
	 * of this page.
	 */

	options = {};
	indexurl = index;

	for (var hash in hash_list) {
		// Download jscalendar-options.js
		var xhr_object = null; 
		if(window.XMLHttpRequest) // Firefox 
			xhr_object = new XMLHttpRequest(); 
		else if(window.ActiveXObject) // Internet Explorer 
			xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
		else { // XMLHttpRequest non supporté par le navigateur 
			return; 
		} 

		xhr_object.open("GET", base + hash_list[hash] + '/jscalendar-options.js', false);
		xhr_object.send(null); 
		if (xhr_object.status == 404) {
			/* Do nothing */
		} else {
			// Parse file
			options[hash_list[hash]] = JSON.parse(xhr_object.responseText);
		}
	}
}
