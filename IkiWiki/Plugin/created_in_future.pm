#!/usr/bin/perl
use strict;
use warnings;
use POSIX;
use Time::Local;

package IkiWiki::Plugin::created_in_future;

=head1 NAME

IkiWiki::Plugin::created_in_future - Add a pagespec to match pages created in
the future (creation date greater than current day).

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::created_in_future

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

IkiWiki::Plugin::created_in_future - Add a pagespec to match pages created in
the future (creation date greater than current day).

See doc/plugins/created_in_future.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/created_in_future
http://ikiwiki.info/plugins/contrib/created_in_future/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2012-2013 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut


use IkiWiki '3.00';

sub import {
	hook(type => "needsbuild", id => "created_in_future", call => \&needsbuild);
	hook(type => "checkconfig", id => "created_in_future", call => \&checkconfig);
}

sub checkconfig() {
  if (not defined($config{timezone})) {
    error("Error: A timezone must be defined using configuration option 'timezone'.");
  }
}

sub needsbuild (@) {
	my $needsbuild=shift;
	my $today = DateTime->today;
	foreach my $page (keys %pagestate) {
		if (exists $pagestate{$page}{created_in_future}{nextchange}) {
			my $nextchange = DateTime->from_epoch(epoch=>$pagestate{$page}{created_in_future}{nextchange}, time_zone => $config{timezone});
			if ($nextchange < $today) {
				# force a rebuild so the created_in_future shows
				# the current day
				delete $pagestate{$page}{created_in_future}{nextchange};
				push @$needsbuild, $pagesources{$page};
			}
			if (exists $pagesources{$page} && 
					grep { $_ eq $pagesources{$page} } @$needsbuild) {
				# remove state, will be re-added if
				# the created_in_future is still there during the
				# rebuild
				delete $pagestate{$page}{created_in_future};
			}
		}
	}
	return $needsbuild;
}

package IkiWiki::PageSpec;

use IkiWiki '3.00';
use DateTime;

sub min ($$) { $_[$_[0] > $_[1]] }

sub match_created_in_future($$;@) {
	# Get argument as an array (year, month, day)
	my ($page, $pagespec, %named) = @_;
	# We are only interested in day. We get rid of time.
	# There must be a simplest way to do this.
	my $day = DateTime->from_epoch(epoch => $IkiWiki::pagectime{$page}, time_zone => $config{timezone})->truncate(to=>'day');
	my $today = DateTime->today(time_zone => $config{timezone});
	if ($day->epoch() < $today->epoch()) {
		return IkiWiki::FailReason->new("Page created in the past");
	} else {
		if (exists $named{'location'}) {
			my $parent = $named{'location'};
			if (exists $pagestate{$parent}{created_in_future}{nextchange}) {
				$pagestate{$parent}{created_in_future}{nextchange} = min(
					$pagestate{$parent}{created_in_future}{nextchange},
					$day->epoch()
				);
			} else {
				$pagestate{$parent}{created_in_future}{nextchange} = $day->epoch();
			}
		}
		return IkiWiki::SuccessReason->new("Page created in the future");
	}
}

1
