#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::jscalendar;

=head1 NAME

IkiWiki::Plugin::jscalendar - IkiWiki plugin to display calendars, in javascript.

=head1 VERSION

This describes version B<0.3> of IkiWiki::Plugin::jscalendar

=cut

our $VERSION = '0.3';

=head1 DESCRIPTION

Javascript calendar.

See doc/plugins/jscalendar.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/jscalendar
http://ikiwiki.info/plugins/contrib/jscalendar/

=head1 AUTHOR

Manoj Srivastava wrote the original Ikiwiki::plugin::calendar plugin. Almost no
line remain from this original plugin.

Louis Paternault (spalax) <spalax at gresille dot org> improved it to use javascript.

=head1 COPYRIGHT

Copyright (c) 2006, 2007 Manoj Srivastava <srivasta at debian dot org>
Copyright (c) 2012-2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;
use IkiWiki::Render;
use Time::Local;
use POSIX qw/setlocale LC_TIME strftime/;
use Storable qw(freeze thaw);

# When $refresh is set to 1, an additional refresh is needed.
our $refresh = 0;

sub quote {
	my $str = $_[0];
	$str =~ s/(["\\])/\\$1/g;
	return $str;
}

# Extension of jscalendar dummy files handling dependencies.
my $ext="jscalendar";

sub import {
	hook(type => "getsetup", id => "jscalendar", call => \&getsetup);
	hook(type => "preprocess", id => "jscalendar", call => \&preprocess);
	hook(type => "format", id => "jscalendar", call => \&format);
	hook(type => "htmlize", id => "$ext", call => \&htmlize);
	hook(type => "rendered", id => "jscalendar", call => \&rendered);
	hook(type => "needsbuild", id => "jscalendar", call => \&needsbuild);
}

# Hashes of strings that are populated during preprocess() calls, and added in
# each HTML pages by the format() calls.
our %local_firstparsedate;
our %local_init;

sub needsbuild (@) {
	my $needsbuild = shift;

	for my $page (@$needsbuild) {
		for my $hash (keys %{$wikistate{jscalendar}{depends}}) {
			if (exists $wikistate{jscalendar}{depends}{$hash}{$page}) {
				# It will be re-set while building $page.
				delete $wikistate{jscalendar}{depends}{$hash}{$page};
			}
		}
	}

	return $needsbuild;
}

sub format (@) {
	# Insert javascript code
	my %params=@_;

	# Include code right after <body>
	my $includejs = "<script src='" . urlto("ikiwiki/ikiwiki.js", $params{page}) . "' type='text/javascript' charset='utf-8'></script>";
	$includejs .= "<script src='" . urlto("jscalendar/jscalendar.js", $params{page}) . "' type='text/javascript' charset='utf-8'></script>";
	if (! ($params{content}=~s!^(<body[^>]*>)!$1.$includejs!em)) {
		# no <body> tag, probably in preview mode
		$params{content}=$includejs.$params{content};
	}

	# Include code just before </body>
	$local_firstparsedate{$params{page}} = "" if not exists $local_firstparsedate{$params{page}};
	$local_init{$params{page}} = {} if not exists $local_init{$params{page}};
	$includejs = '<script type="text/javascript">' . "\n";
	$includejs .= 'function initjscalendar_noargs() {' . "\n";
	$includejs .= sprintf('    initjscalendar("%s", "%s", [%s]);',
		urlto("jscalendar",$params{page}),
		urlto("/", $params{page}),
		join(", ", map {"\"$_\""} (keys %{$local_init{$params{page}}}))
	) . "\n";
	$includejs .= $local_firstparsedate{$params{page}};
	$includejs .= "}\n";
	$includejs .= 'hook("onload", initjscalendar_noargs)' . "\n";
	$includejs .= '</script>';
	if (! ($params{content}=~s!^(</body[^>]*>)!$includejs.$1!em)) {
		# no <body> tag, probably in preview mode
		$params{content}=$includejs.$params{content};
	}

	return $params{content};
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
		archivebase => {
			type => "string",
			example => "archives",
			description => "base of the archives hierarchy",
			safe => 1,
			rebuild => 1,
		},
		archive_pagespec => {
			type => "pagespec",
			example => "page(posts/*) and !*/Discussion",
			description => "PageSpec of pages to include in the archives; used by ikiwiki-jscalendar command",
			link => 'ikiwiki/PageSpec',
			safe => 1,
			rebuild => 0,
		},
		css_jscalendar_keyword => {
			type => "string",
			example => "calendar",
			description => "keyword for css classes (default is calendar, resulting in the jscalendar looking like the calendar from the calendar plugin.",
			safe => 1,
			rebuild => 1,
		},
		week_start_day => {
			type => "integer",
			example => 1,
			description => "first day of the week (0 is Sunday)",
			safe => 1,
			rebuild => 1,
		},
		month_link => {
			type => "integer",
			example => "0",
			description => "Month are links to archivebase/year/month pages (0 is false, 1 is true).",
			safe => 1,
			rebuild => 1,
		},
}

# Each jscalendar must have a different id. This variable is used to count the
# number of such calendars.
our $jscalendarid=0;

sub preprocess (@) {
	# Place the right <div> in place of the calendar. It will be updated as a
	# real calendar by the javascript code.
	#
	# Mark all .js files to be rendered.
	my %params=@_;

	$params{year} = "" unless defined $params{year};
	$params{month} = "" unless defined $params{month};

	my $pagespec = '';
	$pagespec = $config{archive_pagespec} if defined $config{archive_pagespec};
	$pagespec = $params{pages} if defined $params{pages};

	my $archivebase = '';
	$archivebase = $config{archivebase} if defined $config{archivebase};
	$archivebase = $params{archivebase} if defined $params{archivebase};

	my $type = "month";
	$type = $params{type} if defined $params{type};

	eval q{use Digest::MD5};
	error($@) if $@;
	my $hash=IkiWiki::possibly_foolish_untaint(
		Digest::MD5::md5_hex($archivebase) .
		Digest::MD5::md5_hex($pagespec)
	);
	$wikistate{jscalendar}{depends}{$hash}{$pagesources{$params{page}}} = 1;
	my $directory = "$IkiWiki::Plugin::transient::transientdir/jscalendar";
	my %to_freeze = (
		hash => $hash,
		page => $params{page},
		archivebase => $archivebase,
		pagespec => $pagespec,
	);

	writefile("$hash.$ext", $directory, freeze(\%to_freeze));
	add_depends("jscalendar/$hash", $params{page});
	if (! exists $destsources{"jscalendar/$hash.$ext"}) {
		$refresh = 1;
	}

	my $calendar = "";
	if ($params{type} eq "month") {
		$calendar .= '<div class="jscalendar-month" id="'. "jscalendar$jscalendarid" .'"></div>';
		if (not exists $local_firstparsedate{$params{destpage}}) {
			$local_firstparsedate{$params{destpage}} = "";
		}
		$local_firstparsedate{$params{destpage}} .= "    jscalendar_parse_date('$hash', 'jscalendar$jscalendarid', '$params{year}', '$params{month}');\n";
		$jscalendarid += 1;
	} elsif ($params{type} eq "year") {
			# Return an html year javascript calendar.
			error("Not implemented yet. I will implement it when I need it, unlikely before ;)");
	}

	$local_init{$params{destpage}}{$hash} = 1;

	return "\n<div class=\"calendar\">$calendar</div>\n";
}

sub previousmonth ($$@) {
	my $year = shift;
	my $month = shift;
	my @monthlist = @_;

	my $mitem;
	foreach my $tuple (reverse @monthlist) {
		my ($yitem, $mitem) = @{$tuple};
		if (($yitem == $year and $mitem < $month) or ($yitem < $year)) {
			return "[$yitem, $mitem]";
		}
	}

	return "false";
}

sub nextmonth ($$@) {
	my $year = shift;
	my $month = shift;
	my @monthlist = @_;

	my $mitem;
	foreach my $tuple (@monthlist) {
		my ($yitem, $mitem) = @{$tuple};
		if (($yitem == $year and $mitem > $month) or ($yitem > $year)) {
			return "[$yitem, $mitem]";
		}
	}

	return "false";
}

sub htmlize(@) {
	my %params=@_;
	my %content = %{thaw($params{content})};
	my $hash = $content{hash};

	if (!keys %{$wikistate{jscalendar}{depends}{$content{hash}}}) {
		# This calendar is no longer used.
		return "";
	}


	# Gathering list of posts
	my %posts;
	foreach my $page (pagespec_match_list("jscalendar/$hash", $content{pagespec})) {
		my @date  = localtime($IkiWiki::pagectime{$page});
		my $day  = $date[3];
		my $month = $date[4] + 1;
		my $year  = $date[5] + 1900;
		my $title;
		if (exists $pagestate{$page}{meta}{title}) {
			$title = $pagestate{$page}{meta}{title};
		} else {
			$title = IkiWiki::basename($page);
		}
		$posts{$year}{$month}{$day} = () unless exists $posts{$year}{$month}{$day};
		push(@{$posts{$year}{$month}{$day}}, sprintf('{"url": "%s", "title": "%s"}', 
				urlto($page, "index"),
				quote($title),
			));
	}

	# Building list of months
	my @monthlist;
	foreach my $year (keys %posts) {
		foreach my $month (keys %{$posts{$year}}) {
			push @monthlist, [$year, $month];
		}
	}
	my @sortedmonths = sort {sprintf("$a->[0]%02d", $a->[1]) cmp sprintf("$b->[0]%02d", $b->[1])} @monthlist;

	# Building jscalendar-options.js
	my $css_jscalendar_keyword = 'calendar';
	$css_jscalendar_keyword = $config{css_jscalendar_keyword} if defined $config{css_jscalendar_keyword};
	my $week_start_day = 0;
	$week_start_day = $config{week_start_day} if defined $config{week_start_day};
	my $month_link = 0;
	$month_link = $config{month_link} if defined $config{month_link};
	$month_link = IkiWiki::yesno($month_link);

	my $file = "{\n";
	$file .= "  \"css_jscalendar_keyword\": \"" . quote($css_jscalendar_keyword) . "\",\n";
	$file .= "  \"week_start_day\": $week_start_day,\n";
	if ($month_link) {
		$file .= "  \"month_link\": true,\n";
	} else {
		$file .= "  \"month_link\": false,\n";
	}
	$file .= sprintf('  "archivebase": "%s",', urlto($content{archivebase}, "index")) . "\n";
	$file .= "  \"day_name\": [ ";
	foreach my $day (1..7) {
		# Year 1989 started with a Sunday
		if ($day != 1) {
		$file .= ", ";
		}
		$file .= '"' . quote(strftime_utf8('%A', localtime(timelocal(0, 0, 0, $day, 0, 89)))) . '"';
	}
	$file .= " ],\n";
	$file .= "  \"month_name\": [ ";
	foreach my $month (0..11) {
		if ($month != 0) {
			$file .= ", ";
		}
		$file .= '"' . quote(strftime_utf8('%B', localtime(timelocal(0, 0, 0, 1, $month, 0)))) . '"';
	}
	$file .= "  ],\n";
	$file .= "  \"month_list\": [ ";
	$file .= join(", ", map {"[$_->[0], $_->[1]]" } @sortedmonths);
	$file .= " ]\n";
	$file .= "}\n";
	will_render($params{page}, "jscalendar/$hash/jscalendar-options.js");
	writefile("jscalendar/$hash/jscalendar-options.js", $config{destdir}, $file);

	# Building month files
	foreach my $year (keys %posts) {
		foreach my $month (keys %{$posts{$year}}) {
			my $file = "";
			$file .= "{\n";
				$file .= sprintf('  "adjacent": [%s, %s],', previousmonth($year, $month, @sortedmonths), nextmonth($year, $month, @sortedmonths)) . "\n";
				$file .= '  "content": {' . "\n";
				my $first = 1;
				foreach my $day (keys %{$posts{$year}{$month}}) {
					if (not $first){
						$file .= ",";
					}
					$first = 0;
					$file .= sprintf('    "%s" : [', $day) . "\n      ";
						$file .= join(",\n      ", @{$posts{$year}{$month}{$day}});
					$file .= "\n    ]\n";
				}
				$file .= "  }\n";
			$file .= "}\n";
			will_render($params{page}, sprintf("jscalendar/$hash/jscalendar-$year-%02d.js", $month));
			writefile(sprintf("jscalendar-$year-%02d.js", $month), "$config{destdir}/jscalendar/$hash", $file);
		}
	}


	return "";
}

sub rendered(@) {
	if ($refresh) {
		$refresh = 0;

		# without preliminary saveindex/loadindex, refresh()
		# complains about a lot of uninitialized variables
		IkiWiki::saveindex();
		IkiWiki::loadindex();
		IkiWiki::refresh();
		IkiWiki::saveindex();
	}
}

1
