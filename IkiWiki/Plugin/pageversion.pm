#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::pageversion;

=head1 NAME

IkiWiki::Plugin::pageversion - IkiWiki plugin to manage different versions of the same page

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::pageversion

=cut

our $VERSION = '0.1.0';

=head1 DESCRIPTION

This plugin is intended to manage different versions of the same page, allowing both to:

- make older/newer versions available;
- highlight the latest version.

See doc/plugins/pageversion.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/pageversion
http://ikiwiki.info/plugins/contrib/pageversion/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2016-2017 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

# How version information is stored
#
# Versions of pages are stored using tags. For instance, a page marked as a
# version of 'some/page/foo.mdwn' will be tagged
# '_pageversion*/page/some/page/foo'. Thus, marking a page as a version of
# something is a shortcut to tagging this page with the appropriate tag;
# checking if a page is a version of something is a shortcut to checking if
# this page is tagged with the right tag.


use warnings;
use strict;
use IkiWiki 3.00;
use Encode;

my $add_tag;

sub import {
  IkiWiki::loadplugin("tag");
  $add_tag = \&{$IkiWiki::hooks{preprocess}{tag}{call}};

  hook(type => "getsetup", id => "pageversion", call => \&getsetup);
  hook(type => "preprocess", id => "versionof", call => \&preprocess, scan => 1);
}

sub getsetup () {
  return
  plugin => {
    safe => 1,
    rebuild => undef,
    section => "widget",
  },
}

my $TAGPREFIX = "_pageversion";
my $TAGFORMAT = $TAGPREFIX . "/%1\$s/%2\$s";
my $TAGPAGEPREFIX;
if (defined $config{tagbase}) {
  $TAGPAGEPREFIX = "/$config{tagbase}/$TAGPREFIX";
} else {
  $TAGPAGEPREFIX = "$TAGPREFIX";
}

sub tagname(@) {
  # Given the arguments to directive `versionof`, return the name of the corresponding tag.

  my %params = @_;
  my $page = $params{page};

  my $parent = $params{parent};
  my $name = $params{name};
  my $label = $params{label};

  if (defined $parent) {
    $name = $page;
    $name =~ s+/[^/]*$++;
  }
  if (defined $name and $name eq "") {
    $name = $page;
  }
  if (defined $name) {
    return sprintf($TAGFORMAT, "page", bestlink($page, $name));
  }
  if (defined $label) {
    return sprintf($TAGFORMAT, "label", $label);
  }

  error("Unable to guess tag name from arguments.")
}

sub preprocess (@) {
  # Processing `versionof` directive: tag page with the appropriate tag.

  my %params=@_;
  my $tagname = tagname(
    page => $params{page},
    name => $params{name},
    parent => $params{parent},
    label => $params{label},
  );
  $add_tag->(
    page => $params{page},
    destpage => $params{destpage},
    preview => $params{preview},
    $tagname => '',
  );
  return "";
}

package IkiWiki::PageSpec;

use IkiWiki '3.00';

sub tagname($$) {
  # Given the appropriate arguments, return the tag to be matched to.

  my $page = shift;
  my $against = shift;

  my $name = undef;
  my $parent = undef;
  my $label = undef;

  if ($against eq "parent") {
    $parent = "";
  }
  if ($against eq "name") {
    $name = "";
  }
  if ($against =~ "^name=.*") {
    $name = $against;
    $name =~ s/^name=//
  }
  if ($against =~ "^label=.*") {
    $label = $against;
    $label =~ s/^label=//
  }

  return IkiWiki::Plugin::pageversion::tagname(
    page => $page,
    name => $name,
    parent => $parent,
    label => $label,
  );
}

sub match_versionof($$; @) {
  my $page = shift;
  my $against = shift;
  my %params = @_;

  if ($against eq "any") {
    my $pagespec = sprintf(
      "(tagged(%s) or tagged(%s))",
      sprintf($TAGFORMAT, "page", "*"),
      sprintf($TAGFORMAT, "label", "*"),
    );
    if (pagespec_match($page, $pagespec, %params)) {
      return IkiWiki::SuccessReason->new("Page is a version of some another page.");
    } else {
      return IkiWiki::FailReason->new("Page is not a version of any other page.");
    }
  } else {
    my $tagname;
    eval {
      $tagname = tagname($params{location}, $against);
    };
    if ($@) {
      return IkiWiki::FailReason->new("Invalid arguments.");
    }
    return pagespec_match(
      $page,
      "tagged($tagname)",
      %params,
    );
  }
}

sub match_latest_tagged($$; @) {
  my $page = shift;
  my $tag = shift;
  my %params = @_;

  my $first = (pagespec_match_list($page, "tagged($tag)",
    deptype => deptype("presence"),
    sort => "age",
    num => 1,
    location => $params{location},
    ))[0];
  if ($first eq $page) {
    return IkiWiki::SuccessReason->new("Page is the latest page with the given tag.");
  } else {
    return IkiWiki::FailReason->new("Page is not tagged with the given tag, or there are more recent pages tagged with it.");
  }
}

sub match_latestversion($$; @) {
  my $page = shift;
  my $against = shift;
  my %params = @_;

  if ($against eq "any") {
    my $taglist = defined($typedlinks{$page}{tag}) ? ($typedlinks{$page}{tag})[0] : {};
    foreach my $tag (keys %$taglist) {
      if ($tag =~ m+^$TAGPAGEPREFIX/+) {
        if (match_latest_tagged($page, $tag, %params)) {
          return IkiWiki::SuccessReason->new("Page is the latest version of some other page.");
        }
      }
    }
    return IkiWiki::FailReason->new("Page is not the latest version of any other page.");
  } else {
    return match_latest_tagged(
      $page,
      tagname($params{location}, $against),
      @_,
    );
  }
}

1
