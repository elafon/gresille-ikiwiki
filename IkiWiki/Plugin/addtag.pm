#!/usr/bin/perl

package TagTree;

=head1 NAME

TagTree - A class to store trees of tags

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

sub New ($$;$){
  my ($invocant, $page, $parent)=@_;
  my $object = {
    page => "",
    children => {},
  };
  if ((defined $parent) and ($parent)) {
    $object->{page} = "$parent/$page";
  } else {
    $object->{page} = $page;
  }
  return bless($object,$invocant);
}

sub Add($$$){
  my ($invocant,$text,@path) = @_;
  if (@path) {
    my $child = shift(@path);
    if (not exists($invocant->{children}{$child})) {
      $invocant->{children}{$child} = TagTree->New($child, $invocant->{page});
    }
    $invocant->{children}{$child}->Add($text, @path);
  } else {
    $invocant->{text} = $text;
  }
}

sub as_ul($) {
  my ($invocant) = @_;
  my $html = "";
  $html.= "<ul>\n";
  foreach my $child (sort(keys(%{$invocant->{children}}))) {
    $html.= "<li>";
    if ($invocant->{children}{$child}->{text}) {
      $html.= "<a href=\\\"#\\\" onclick=\\\"insert_tag(&quot;$invocant->{children}{$child}->{page}&quot;); return false\\\">" . join('&quot;', split('"', $invocant->{children}{$child}->{text})) . "</a>";
    }
    $html.= $invocant->{children}{$child}->as_ul();
    my $random = rand();
    $html.= "</li>\n";
  }
  $html.= "</ul>\n";
  return $html;
}

1;

package IkiWiki::Plugin::addtag;

=head1 NAME

IkiWiki::Plugin::addtag - Tag page by clicking on the corresponding page.

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::addtag

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

Add the necessary javascript to allow users tagging pages by clicking on tags,
while in the edit page.

The editpage template has to be modified to enable this plugin.

See doc/plugins/addtag.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/addtag
http://ikiwiki.info/plugins/contrib/addtag/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;
use HTML::Template;

sub import {
	hook(type => "getsetup", id => "addtag", call => \&getsetup);
	hook(type => "rendered", id => "addtag", call => \&buildjs);
	hook(type => "delete", id => "addtag", call => \&buildjs);
	hook(type => "formbuilder", id => "addtag", call => \&formbuilder, last => 1);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => 0,
			section => "web",
		},
}

sub formbuilder (@) {
	my %params=@_;
	my $form=$params{form};
	my $q=$params{cgi};

	return if ! defined $form->field("do") || ($form->field("do") ne "edit" && $form->field("do") ne "create") ;
	my $includejs = "<script src='" . urlto("ikiwiki/ikiwiki.js", $params{page}) . "' type='text/javascript' charset='utf-8'></script>";
	$includejs .= "<script src='" . urlto("ikiwiki/toggle.js", $params{page}) . "' type='text/javascript' charset='utf-8'></script>";
	$includejs .= "<script src='" . urlto("") . "javascript/addtag.js" . "' type='text/javascript' charset='utf-8'></script>";
  $includejs .= "<script type='text/javascript' charset='utf-8'>
      hook('onload', initaddtag);
  </script>";

	$form->tmpl_param("ADDTAG" => $includejs);
}

sub tagbase_pagespec() {
  if (defined $config{tagbase}) {
    return  $config{tagbase} . "/*";
  } else {
    return "*";
  }
}

sub buildjs (@) {
  my @pages = @_;
  my $rebuild = 0;
  my $pagespec = tagbase_pagespec();
  for my $page (@pages) {
    if (pagespec_match($page, $pagespec)) {
      $rebuild = 1;
      last;
    }
  }
  if ($rebuild) {
    my $Tree = TagTree->New('');
    for my $page (keys %pagesources) {
      if (pagespec_match($page, $pagespec)) {
        $Tree->Add(($page =~ s/.*\///r), split('/', (IkiWiki::Plugin::tag::tagname("/" . $page) =~ s/ /_/rg )));
      }
    }

    my @html = <<"__EOD__";
function insert_tag(tagname) {
  textarea = document.getElementById("editcontent");
  textarea.value += '\n[[!tag ' + tagname + ']]';
  textarea.scrollTop = textarea.scrollTopMax;
}

function tags_as_ul() {
  return "<em>To add a new tag :</em> <code>[[!tag new_tag]]</code> <TMPL_VAR TAGTREE>";
}

function initaddtag() {
  var link = document.getElementById('addtag');
  link.innerHTML = tags_as_ul();
}
__EOD__

    # Writing javascript file
    my $t = HTML::Template->new(arrayref => \@html);
    $t->param(tagtree => join("\\\n", split("\n", $Tree->as_ul() . "\n")));
    writefile("addtag.js", $config{destdir} . "/javascript", $t->output());
  }
}

1
