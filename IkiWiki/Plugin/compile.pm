#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::compile;

=head1 NAME

IkiWiki::Plugin::compile - Compile files on the fly, and publish their compiled version

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::compile

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

See doc/plugins/compile.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/compile
http://ikiwiki.info/plugins/contrib/compile/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;
use File::Basename;
use File::Copy;
use File::Path;
use File::Temp;
use JSON;
use String::Formatter stringf => {
  input_processor => 'require_named_input',
  string_replacer => 'named_replace',

  codes => {
    s => sub { $_ },     # string itself
  },
};

sub make_path_error($) {
  my $err = shift;
  my $message = "";
  for my $diag (@$err) {
    my ($file, $message) = %$diag;
    if ($file eq '') {
      $message .= "General error: $message.\n";
    } else {
      $message .= "Problem unlinking $file: $message.\n";
    }
  }
}

sub import {
	hook(type => "getsetup", id => "compile", call => \&getsetup);
	hook(type => "preprocess", id => "compile", call => \&preprocess);
	hook(type => "checkconfig", id => "compile", call => \&checkconfig);
	hook(type => "needsbuild", id => "compile", call => \&needsbuild);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
}

sub needsbuild() {
  my $list = shift;
  foreach my $file (keys %{$wikistate{compile}{built}}) {
    if (grep {/^$file$/} @$list) {
      delete $wikistate{compile}{built}{$file};
    }
  }
}

sub checkconfig() {
	$config{compile_source} = 1  unless defined $config{compile_source};
	$config{compile_template_source} = "compile_source.tmpl"  unless defined $config{compile_template_source};
	$config{compile_template_nosource} = "compile_nosource.tmpl"  unless defined $config{compile_template_nosource};
  $config{compile_tmpdir} = File::Spec->join($config{srcdir}, ".ikiwiki", "tmp", "compile") unless defined $config{compile_tmpdir};
  $config{compile_inline} = 0 unless defined $config{compile_inline};

  $config{compile_filetypes} = "{}" unless defined $config{compile_filetypes};
  eval {
    $config{compile_filetypes_hash} = from_json($config{compile_filetypes})
  };
  if ($@) {
    error("Could not parse 'compile_filetypes' as JSON: $@\nString was $config{compile_filetypes}.");
  }

  if (defined $config{compile_bindir}) {
    if (not File::Spec->file_name_is_absolute($config{compile_bindir})) {
      $config{compile_bindir} = File::Spec->join($config{srcdir}, $config{compile_bindir});
    }
  }

  foreach my $filetype (keys %{$config{compile_filetypes_hash}}) {
    # Set default parameters of extensions
    foreach my $parameter (qw(source template_nosource template_source build depends inline destname)) {
      $config{compile_filetypes_hash}{$filetype}{$parameter} = get_parameter($parameter, $filetype);
    }
    # Special process for destname
    if (ref \$config{compile_filetypes_hash}{$filetype}{destname} eq "SCALAR") {
      $config{compile_filetypes_hash}{$filetype}{destname} = {
        'file' => $config{compile_filetypes_hash}{$filetype}{destname},
      };
    } elsif (ref $config{compile_filetypes_hash}{$filetype}{destname} eq "HASH") {
      ;
    } else {
      error("Could not parse 'destname' for '$filetype': must be string or hash of strings: $config{compile_filetypes_hash}{$filetype}{destname}");
    }
    # Special process for build
    if ((not defined $config{compile_filetypes_hash}{$filetype}{build})  and (defined $config{compile_bindir})) {
      if (-x File::Spec->join($config{compile_bindir}, $filetype)) {
        $config{compile_filetypes_hash}{$filetype}{build} = File::Spec->join($config{compile_bindir}, $filetype) . ' "%{srcname}s"';
      } elsif (-r File::Spec->join($config{compile_bindir}, "make.$filetype")) {
        my $arguments = "";
        foreach my $file (keys %{$config{compile_filetypes_hash}{$filetype}{destname}}) {
          $arguments .= " '%{dest_$file}s'";
        }
        $config{compile_filetypes_hash}{$filetype}{build} = 'make -f "' . File::Spec->join($config{compile_bindir}, "make.$filetype") . '" ' . $arguments;
      } else {
        $config{compile_filetypes_hash}{$filetype}{build} = undef;
      }
    }
  }

  # Make wikilink render files with certain extensions
  my $orightmllink=\&htmllink;
  inject(name => 'IkiWiki::htmllink', call => sub {
    my $linkextension = (splitpath($_[2]))[2];
    if (defined $config{compile_filetypes_hash}{$linkextension}{inline} and IkiWiki::yesno($config{compile_filetypes_hash}{$linkextension}{inline})) {
      return preprocess(
        page=> $_[0],
        destpage=> $_[1],
        files=> $_[2],
      );
    }
    return $orightmllink->(@_);
  });

  if (not -d $config{compile_tmpdir}) {
    File::Path::make_path($config{compile_tmpdir}, {error => \my $err});
    if (@$err) {
      $config{compile_error} = "Cannot create path \"$config{compile_tmpdir}\": " . make_path_error($err);
    }
  }
}

sub get_parameter($$) {
  my ($parameter, $filetype) = @_;
  if (defined $config{compile_filetypes_hash}{$filetype}{$parameter}) {
    return $config{compile_filetypes_hash}{$filetype}{$parameter};
  }
  return $config{"compile_" . $parameter};
}

sub begins_with {
  return substr($_[0], 0, length($_[1])) eq $_[1];
}

sub splitpath($) {
  my $path = shift;

  # Directory/File
  my $dirname = "";
  my $filename = $path;
  if (index($path, "/") != -1) {
    ($dirname, $filename) = $path =~ /^(.*)\/([^\/]*)$/;
  }

  # Extension
  my $extension = "";
  my $basename = $filename;
  if (index($filename, ".") != -1) {
    ($basename, $extension) = $filename =~ /^(.*)\.([^.]*)$/;
  }

  return ($dirname, $basename, $extension);
}

# wikiname($page, $file)
# Return the path to $file (from page $page). This path is relative to the wiki
# source directory.
sub wikiname($$) {
  my ($page, $file) = @_;
  my $name = bestlink($page, $file);
  if ($name eq "") {
    return substr(srcfile($file), length($config{srcdir})+1);
  } else {
    return $name;
  }
}

sub preprocess (@) {

	my %params=@_;
	if ($config{compile_error}) {
		error($config{compile_error});
	}

  my $page = $params{page};
  my $destpage = $params{destpage};

  # Files
  $params{files} = "" if not defined $params{files};
  my @depends = split(/ +/, $params{files});
  foreach my $file (@depends) {
    add_depends($page, $file);
  }
  my $origname = shift(@depends);
  if (not defined $origname) {
    error("Missing file name.");
  }

  # Extension
  my ($origdirname, $basename, $srcextension) = splitpath($origname);

  # File type
  my $filetype = $srcextension;
  $filetype = $params{filetype} if defined $params{filetype};

  ### Path names
  # Example (with page "sub/page" containing command [[!compile file="dir/file.ext"]]:
  # - Path of source wiki directory: /home/ikiwiki/wiki
  # - Page containing the command: /home/ikiwiki/wiki/sub/page
  # - File name: /home/ikiwiki/wiki/sub/page/dir/file.ext
  # - Destination name: file.dst
  # - Temp directory: tmpdir
  #
  # - srcwiki:  $config{srcdir}
  # - dirname: sub/page
  # - origdirname: dir
  # - basename: file
  # - srcextension: ext
  # - srcname: $basename.$srcextension
  # - wikiname: path from $config{srcdir}
  # - srcfullname: $config{srcdir}/$wikiname
  #
  # - destwikiname: hash of $dirname/$origdirname/$destname for each of the
  #   destination names (same structure as option $destname).
  # - destwiki: $config{destdir}
  # - destfullname: path from file system root

  my $wikiname = wikiname($page, $origname);
  my $dirname = substr($wikiname, 0, -length($origname));
  my $srcname = "$basename.$srcextension";
  my $srcfullname = File::Spec->join($config{srcdir}, $wikiname);

  # Gathering options
  my %local_options = (
    'srcwiki' => $config{srcdir},
    'srcextension' => $srcextension,
    'filetype' => $filetype,
    'dirname' => $dirname,
    'wikiname' => $wikiname,
    'srcfullname' => $srcfullname,
    'srcname' => $srcname,
    'basename' => $basename,
    'destwiki' => $config{destdir},
  );

  # Destname
  my %destname;
  if (defined $params{destname}) {
    %destname = (
      "file" => $params{destname}
    );
  } else {
    %destname = %{$config{compile_filetypes_hash}{$filetype}{destname}};
    foreach my $file (keys %destname) {
      $destname{$file} = stringf($destname{$file}, \%local_options);
    }
  }

  my %destwikiname = ();
  foreach my $file (keys %destname) {
    $destwikiname{$file} = File::Spec->join($dirname, $origdirname, $destname{$file});
    $local_options{"dest_${file}"} = $destname{$file};
    $local_options{"dest_${file}_wikiname"} = $destwikiname{$file};
    $local_options{"dest_${file}_fullname"} = File::Spec->join($config{destdir}, $destwikiname{$file});
  }
  my $destdir = File::Spec->join($config{destdir}, $dirname, $origdirname);

  foreach my $key (keys %params) {
    if (begins_with($key, "var_")) {
      my $varname = substr($key, 4);
      $local_options{$varname} = $params{$key};
    }
  }

  # Display source?
  my $source;
  if (defined $params{source}) {
    $source = IkiWiki::yesno($params{source});
  } else {
    $source = IkiWiki::yesno($config{compile_filetypes_hash}{$filetype}{source});
  }

  # Name of template to use
  my $templatename;
  if (defined $params{template}) {
    $templatename = $params{template};
  } elsif ($source) {
    $templatename = $config{compile_template_source};
    $templatename = $config{compile_filetypes_hash}{$filetype}{template_source} if (defined $config{compile_filetypes_hash}{$filetype}{template_source});
  } else {
    $templatename = $config{compile_template_nosource};
    $templatename = $config{compile_filetypes_hash}{$filetype}{template_nosource} if (defined $config{compile_filetypes_hash}{$filetype}{template_nosource});
  }
  $templatename =  stringf($templatename, \%local_options);

  # Build command
  my $command_build = $config{compile_filetypes_hash}{$filetype}{build};
  $command_build = $params{build} if defined $params{build};
  $command_build = stringf($command_build, \%local_options);

  if (not exists($wikistate{compile}{built}{$wikiname}{$command_build})) {
    # Creating destination directory
    File::Path::make_path($destdir, {error => \my $err});
    if (@$err) {
      error("Error while creating directory '$destdir': " . make_path_error($err));
    }

    # Copying files
    my $builddir;
    eval {
      $builddir = File::Temp->newdir(
        TEMPLATE => 'tempXXXX',
        DIR => "$config{compile_tmpdir}",
        CLEANUP => 1,
      )
    };
    if ($@) {
      error("Error while creating temporary file: $@");
    }
    foreach my $file (($origname), @{$config{compile_filetypes_hash}{$filetype}{depends}}, @depends) {
      if (not copy(File::Spec->join($config{srcdir}, wikiname($page, $file)), $builddir)) {
        error("Error while copying file '$file' to '$builddir': $!");
      }
      if ($file eq $origname) {
        # Copying source file
        will_render($page, $wikiname);
        my $copysource = File::Spec->join($config{srcdir}, $wikiname);
        my $copydest = File::Spec->join($config{destdir}, $dirname, $origdirname);
        if (not copy($copysource, $copydest)) {
            error("Error while moving file '$copysource' to '$copydest': $!");
          }
      }
    }

    # Running command
    debug("[compile] Running command \"$command_build\"...");
    my $stdout = qx((cd $builddir && $command_build) </dev/null 2>&1);
    if (($? >> 8) != 0) {
      debug("[compile] Failed: $stdout");
      error("Command '$command_build' returned non-zero status : $stdout.");
    }

    # Move compiled file
    foreach my $file (keys %destname) {
      will_render($wikiname, $destwikiname{$file});
      my $copysource = File::Spec->join($builddir, $destname{$file});
      my $destsource = File::Spec->join($config{destdir}, $destwikiname{$file});
      if (not move($copysource, $destsource)) {
          error("Error while moving file '$copysource' to '$destsource': $!");
      }
    }

    # Mark file as built
    $wikistate{compile}{built}{$wikiname}{$command_build} = 1;
  } else {
    will_render($page, $wikiname);
    foreach my $target (values %destwikiname) {
      will_render($wikiname, $target);
    }
  }

  # Return link
  my $template = template_depends($templatename, $page);
  if ($template->query(name => "SRCURL")) {
    $template->param(SRCURL => urlto("$wikiname", $destpage));
  }
  while( my( $key, $value ) = each %local_options ){
    if ($template->query(name => $key)) {
      $template->param($key => $value);
    }
  }
  foreach my $file (keys %destname) {
    my $key = "dest_${file}_url";
    if ($template->query(name => $key)) {
      $template->param($key => urlto($destwikiname{$file}, $destpage));
    }
  }
  foreach my $key (keys %params) {
    if (begins_with($key, "var_")) {
      my $varname = substr($key, 4);
      if ($template->query(name => $varname)) {
        $template->param($varname => $params{$key});
      }
    }
  }

  return $template->output;
}

1
