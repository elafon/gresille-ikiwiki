#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::redirect;

=head1 NAME

IkiWiki::Plugin::redirect - IkiWiki plugin to redirect pages

=head1 VERSION

This describes version B<0.1.0> of IkiWiki::Plugin::redirect

=cut

our $VERSION = '0.1.0';

=head1 DESCRIPTION

Redirect a page to another one, defined using a pagespec.

See doc/plugins/redirect.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/redirect
http://ikiwiki.info/plugins/contrib/redirect/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

Some lines of code have been copied from the inline plugin.

=head1 COPYRIGHT

Copyright (c) 2016 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut


use warnings;
use strict;
use Encode;
use IkiWiki 3.00;
use URI;

my %redirect;
my $templatename = "redirect.tmpl";

sub import {
	hook(type => "format", id => "redirect", call => \&format);
	hook(type => "preprocess", id => "redirect", call => \&preprocess);
}

sub format (@) {
	# Insert redirect code if necessary
	my %params=@_;

	if (exists $redirect{$params{page}}) {
		my $includemeta = "<meta http-equiv=\"refresh\" content=\"0; url=$redirect{$params{page}}\" />\n";
		$params{content}=~s!^(</head[^>]*>)!$includemeta.$1!em;
	}

	return $params{content};
}

sub preprocess (@) {
	my %params=@_;
	
	if (! exists $params{pages}) {
		error gettext("missing pages parameter");
	}

	my @list;

	my $num=0;
	if ($params{skip} && $num) {
		$num+=$params{skip};
	}

	@list = pagespec_match_list($params{page}, $params{pages},
		deptype => deptype("presence"),
		filter => sub { $_[0] eq $params{page} },
		sort => exists $params{sort} ? $params{sort} : "age title",
		reverse => IkiWiki::yesno($params{reverse}),
		($num ? (num => $num) : ()),
	);

	if (exists $params{skip}) {
		@list=@list[$params{skip} .. $#list];
	}
	
	$redirect{$params{page}} = urlto($list[0], $params{page});

	# Fill template
	my $template = template_depends($templatename, $params{page});
	if ($template->query(name => "TARGETPAGE")) {
		$template->param(TARGETPAGE => $redirect{$params{page}});
	}
	return $template->output;
}

1
