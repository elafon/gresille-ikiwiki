[[!meta title="Test of poetry plugin"]]

[[!poetry content="""
This is a verse
Made of several lines

> And here is the chorus
> La la la!
> A beautiful chorus

Another verse
A bit longer
Than the previous one

) This one is deleted
) Because I did not like it
"""]]
